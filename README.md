## Easy invitation forms

### We hold variety of invitation forms with us

Looking for invitation forms? Use our invitation template for a fast and easy way to make sure you’re asking the right questions, streamlining the set up process. 

#### Our features:

* A/B Testing
* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

### Use our invitation template and customize it to your heart’s content
By starting with our [invitation forms](https://formtitan.com/forms/Health-Lifestyle/Wedding-Invitation-Forms), you’ll be able to share your form and collect entries within minutes.

Happy inviation forms!